import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point1 = new MovablePoint(5, 6);
        MovablePoint point2 = new MovablePoint(2, 4);
        System.out.println(point1);
        System.out.println(point2);
        
        point1.moveDown();
        point1.moveRight();
        System.out.println(point1);


        point2.moveRight();
        point2.moveUp();
        point2.moveUp();
        System.out.println(point2);

    }
}
